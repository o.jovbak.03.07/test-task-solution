(function($) {

    $(document).ready(function () {
        console.log('init custom script');
        let terms = [];
        getNews(1, terms);

        $(document).on('click','.pagination-link ul li',function(){
            if( $(this).attr('p')) {
                let page = $(this).attr('p');
                getNews(page, terms);
            }
        });

        $('.checkbox-filter').on('click', function (e){
            terms = [];
            $('.checkbox-filter:checked').each(function (){
                terms.push(this.value);
            });
            getNews(1, terms);
        })

        $('.clear-filters').on('click', function (e){
            e.preventDefault();
            terms = [];
            $('.checkbox-filter:checked').each(function (){
                $(this).prop('checked', false);
            });
            getNews(1, terms);
        });

        function getNews(page= 1, terms = []){
            let data = {
                'action' : 'get_news_list',
                'page' : page,
            }
            if( terms !== [] ) {
                data.terms = terms;
            }
            $.ajax({
                url: ajax.url,
                type: 'POST',
                data: data,
                success: function (result) {
                    if(result.data) {
                        $('.news-wrapper').html(result.data);
                    }
                },
            });
        }

        $('.show-filters').on('click', function (e){
            e.preventDefault();
            $('.control-group').removeClass("display-none").addClass("display-flex");
            $(this).removeClass("display-block").addClass("display-none");
            $('.hide-filters').removeClass("display-none").addClass("display-block");
        });
        $('.hide-filters').on('click', function (e){
            e.preventDefault();
            $('.control-group').removeClass("display-flex").addClass("display-none");
            $(this).removeClass("display-block").addClass("display-none");
            $('.show-filters').removeClass("display-none").addClass("display-block");
        })

    });
})(jQuery);