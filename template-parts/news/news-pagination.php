<?php
/**
 * @var  $args ;
 */
$curent_page = $args['current_page'];
$page_count = $args['page_count'];
$start_pagination = $curent_page - 3 < 1 ? 1 : $curent_page - 3;
$end_pagination = $curent_page + 3 > $page_count ? $page_count : $curent_page + 3;
?>

<div class='pagination-link'>
    <ul>
        <?php if($curent_page > 1) :
            $pre = $curent_page-1; ?>
            <li p='<?php echo $pre; ?>' class='active'>Previous</li>
        <?php else: ?>
            <li class='inactive'>Previous</li>
        <?php endif;
            for($i = $start_pagination; $i <= $end_pagination; $i++):
                if($i == $curent_page ): ?>
                    <li p='<?php echo $i?>' class = 'selected' ><?php echo $i?></li>
                <?php else: ?>
                    <li p='<?php echo $i?>' class = 'active' ><?php echo $i?></li>
                <?php endif;
            endfor;
        if($curent_page < $page_count):
            $next = $curent_page + 1; ?>
            <li p='<?php echo $next; ?>' class='active'>Next</li>
        <?php else: ?>
            <li class='inactive'>Next</li>
        <?php endif; ?>
    </ul>
</div>
