<?php
/**
 * @var  $args ;
 */

?>

<div class="news-list">
    <?php foreach ($args['posts'] as $post): ?>
        <div class="news-item">
            <h2><a href="<?php echo (get_post_permalink($post))?>"> <?php echo $post->post_title;?> </a></h2>
            <div class="post-excerpt">
                <?php if(!empty($post->post_excerpt)): ?>
                   <p>
                       <?php echo mb_strimwidth($post->post_excerpt, 0, 150, '...');?>
                   </p>
                <?php endif;?>
            </div>
            <div class="post-meta-custom-wrapper">
                <ul class="post-meta">
                    <li class="post-author meta-wrapper">
						<span class="meta-icon">
							<span class="screen-reader-text"><?php _e( 'Post author', 'twentytwenty' ); ?></span>
							<?php twentytwenty_the_theme_svg( 'user' ); ?>
						</span>
                        <span class="meta-text">
                            <?php echo esc_html( get_the_author_meta( 'display_name', $post->post_author) );?>
						</span>
                    </li>
                    <li class="post-date meta-wrapper">
						<span class="meta-icon">
							<span class="screen-reader-text"><?php _e( 'Post date', 'twentytwenty' ); ?></span>
							<?php twentytwenty_the_theme_svg( 'calendar' ); ?>
						</span>
                        <span class="meta-text">
							<?php the_time('Y-m-d H:i'); ?>
						</span>
                    </li>
                    <?php $terms = wp_get_post_terms($post->ID, 'news_types', array('fields' => 'all'));
                    if(!empty($terms)): ?>
                        <li class="post-categories meta-wrapper">
                            <span class="meta-icon">
                                <span class="screen-reader-text"><?php _e( 'Categories', 'twentytwenty' ); ?></span>
                                <?php twentytwenty_the_theme_svg( 'folder' ); ?>
                            </span>
                            <span class="meta-text">
                                <?php foreach ($terms as $key => $term) {
                                    $item = $term->name;
                                    if( (!empty($args['terms_list']) ) && in_array($term->slug, $args['terms_list'])){
                                        $item = '<span class="selected-term">'.$item.'</span>';
                                    }
                                    if($key+1 < count($terms)){
                                        echo $item . ', ';
                                    }
                                    else echo $item;
                                }?>
                            </span>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>

        </div>
    <?php endforeach; ?>
</div>

