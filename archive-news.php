<?php

if ( ! defined( 'ABSPATH' ) ) {
    die( 'No direct script access allowed' );
}

$terms = get_terms(array(
    'taxonomy' => 'news_types',
    'hide_empty' => false,
    'orderby' => 'count',
    'order' => 'DECS',
));
get_header(); ?>
    <section class="news-section">
        <div class="news-container">
            <div class="news-filters">
                <div>
                    <div class="control-group">
                        <h1>Types of News</h1>
                        <?php foreach ($terms as $term):
                            $count = get_term($term)->count;
                            $disabled = $count < 1 ? 'disabled' : '';?>
                            <input class="checkbox-filter" id="<?php echo $term->slug; ?>" value="<?php echo $term->slug; ?>" type="checkbox" <?php echo $disabled; ?>/>
                            <label for="<?php echo $term->slug; ?>">
                                <span></span>
                                    <?php echo $term->name.' ('.$count.')'; ?>
                                <ins><i><?php echo $term->name.' ('.$count.')'; ?></i></ins>
                            </label>
                        <?php endforeach; ?>
                        <button class="clear-filters">Clear filters</button>
                    </div>
                    <button class="show-filters"> <?php twentytwenty_the_theme_svg('chevron-down', 'ui','white'); ?> </button>
                    <button class="hide-filters"> <?php twentytwenty_the_theme_svg('cross', 'ui','white'); ?> </button>
                </div>
            </div>
            <div class="news-wrapper">
            </div>
        </div>
    </section>
<?php
get_footer();